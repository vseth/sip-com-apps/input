FROM ghcr.io/deck9/input:latest

COPY entrypoint.sh .

ENV DB_CONNECTION=mysql

ENTRYPOINT ["./entrypoint.sh"]
