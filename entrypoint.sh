#!/bin/sh

echo "DB_DATABASE=$SIP_MYSQL_INPUT_NAME" >> /var/www/html/.env
echo "DB_HOST=$SIP_MYSQL_INPUT_SERVER" >> /var/www/html/.env
echo "DB_USERNAME=$SIP_MYSQL_INPUT_USER" >> /var/www/html/.env
echo "DB_PASSWORD=$SIP_MYSQL_INPUT_PW" >> /var/www/html/.env
echo "DB_CONNECTION=mysql" >> /var/www/html/.env

/opt/input/start-container.sh
